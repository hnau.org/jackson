package org.hnau.jackson

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectReader
import com.fasterxml.jackson.databind.ObjectWriter


object Json {

    @Suppress("MemberVisibilityCanBePrivate")
    val mapper = com.fasterxml.jackson.module.kotlin.jacksonObjectMapper()

    fun createNode(value: Any): JsonNode =
            mapper.valueToTree(value)

    fun reader(mappingClass: Class<*>): ObjectReader =
            mapper.readerFor(mappingClass)

    inline fun <reified T> reader() =
            reader(T::class.java)

    fun reader(typedReference: TypeReference<*>): ObjectReader =
            mapper.reader().forType(typedReference)

    fun writer(mappingClass: Class<*>): ObjectWriter =
            mapper.writerFor(mappingClass)

    inline fun <reified T> writer() =
            writer(T::class.java)

}