package org.hnau.jackson

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import org.hnau.base.extensions.getGenericClass


open class DelegateJsonDeserializer<T, D>(
        private val delegateToValue: (D) -> T
) : JsonDeserializer<T>() {

    private val delegateClass = javaClass.getGenericClass<D>(1)

    override fun deserialize(
            parser: JsonParser,
            deserializationContext: DeserializationContext
    ) = parser
            .readValueAs(delegateClass)
            .let(delegateToValue)

}