package org.hnau.jackson

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider


open class DelegateJsonSerializer<T, D>(
        private val valueToDelegate: (T) -> D
) : JsonSerializer<T>() {

    override fun serialize(
            value: T,
            generator: JsonGenerator,
            serializers: SerializerProvider
    ) = generator.writeObject(
            valueToDelegate(value)
    )

}